package org.addcel.android.ws.client;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.addcel.android.interfaces.ImageBitmapListener;
import org.addcel.android.interfaces.ImageLoadedListener;
import org.addcel.android.promarine.R;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.widget.ImageView;

public class LoadImage extends AsyncTask<String, Void, Bitmap> {
	private Context con;
	URL url;
	String strUrl;
	private ImageView img;
	private int pos;
	private ImageLoadedListener listener;
	private ImageBitmapListener bmpListener;
	
	public LoadImage(Context con, ImageView img, String strUrl, int pos, ImageLoadedListener listener){
		this.con = con;
		this.img = img;
		this.strUrl = strUrl;
		this.pos = pos;
		this.listener = listener;
	}
	
	public LoadImage(Context con, String strUrl, int pos, ImageBitmapListener bmpListener){
		this.con = con;
		//this.img = img;
		this.strUrl = strUrl;
		this.pos = pos;
		this.bmpListener = bmpListener;
	}
	
	@Override
	protected Bitmap doInBackground(String... urls) {
		// TODO Auto-generated method stub
		//System.out.println("load Image "+pos+" "+strUrl);
		Bitmap bit = null;
		try {
			url = new URL(strUrl);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 try {
				
				
				HttpClient http = new DefaultHttpClient();
				
				HttpGet get = new HttpGet(strUrl);
				
				//post.setEntity(WebUtils.getParamsEntity(params, vals));
				
				HttpResponse rp = http.execute(get);
				
				HttpEntity ent = rp.getEntity();
				InputStream in = ent.getContent();
	            bit = BitmapFactory.decodeStream(in);
		        	            
	           
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch(Exception e){
				e.printStackTrace();
			}
			
			if(bit==null){
				//System.out.println("IMG NULL "+pos);
				  
               try {
            	   HttpURLConnection conn= (HttpURLConnection)url.openConnection();
	               conn.setDoInput(true);
            	   conn.connect();
				  InputStream is = conn.getInputStream();
				  bit = BitmapFactory.decodeStream(is);
				  } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch(Exception e){
					e.printStackTrace();
				}
	             
	               
	              
			}
		return bit;
	}
	
	@Override
	protected void onPostExecute(Bitmap bmp){
		
		if(bmp == null){
			//System.out.println("IMG Loaded es NULL "+pos);
		
			BitmapDrawable bdraw = (BitmapDrawable) con.getResources().getDrawable(R.drawable.ic_launcher);
			bmp = bdraw.getBitmap();
		}else{
			//System.out.println("IMG Loaded (Y) "+pos);
		}
		
		if(img != null)
		 img.setImageBitmap(bmp); 
		 
		 if(listener != null){
			 listener.imageLoaded(img, pos);
		 }else if(bmpListener != null){
			 bmpListener.imageLoaded(bmp, pos);
		 }
		
	}

}
