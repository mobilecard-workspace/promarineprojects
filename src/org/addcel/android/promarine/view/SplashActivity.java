package org.addcel.android.promarine.view;

import org.addcel.android.promarine.R;
import org.addcel.android.version.UrlVersion;
import org.addcel.android.version.VersionTO;
import org.addcel.android.version.VersionWSClient;
import org.addcel.android.version.VersionWSListener;

import android.os.Bundle;

import com.actionbarsherlock.app.SherlockActivity;

public class SplashActivity extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		VersionTO version = new VersionTO("9");
		VersionWSListener listener = new VersionWSListener(SplashActivity.this, InicioActivity.class);
		
		new VersionWSClient(listener, UrlVersion.GET).execute(version);
		
	}
	
	
}
