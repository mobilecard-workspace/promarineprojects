package org.addcel.android.promarine.view;

import java.util.List;

import org.addcel.android.crypto.Cifrados;
import org.addcel.android.dialog.Dialogs;
import org.addcel.android.font.Fonts;
import org.addcel.android.interfaces.Versionable;
import org.addcel.android.mobilecard.constant.Url;
import org.addcel.android.mobilecard.to.LoginReq;
import org.addcel.android.mobilecard.to.PasswordReq;
import org.addcel.android.mobilecard.view.PasswordActivity;
import org.addcel.android.mobilecard.view.RegistroActivity;
import org.addcel.android.promarine.R;
import org.addcel.android.session.SessionManager;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class InicioActivity extends SherlockActivity implements Versionable {

	TextView loginView, passwordView, loginButton;
	EditText loginText, passwordText;
	SessionManager session;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inicio);

		session = new SessionManager(InicioActivity.this);

		initGUI();

	}
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		loginText.setText("");
		passwordText.setText("");
	}

	private void initGUI() {

		loginView = (TextView) findViewById(R.id.view_login);
		passwordView = (TextView) findViewById(R.id.view_password);
		loginButton = (TextView) findViewById(R.id.button_login);
		loginText = (EditText) findViewById(R.id.text_login);
		passwordText = (EditText) findViewById(R.id.text_password);

		int version = android.os.Build.VERSION.SDK_INT;

		if (14 > version)

			setLowerVersionContent(loginView, passwordView, loginButton,
					loginText, passwordText);

	}

	@Override
	public void setLowerVersionContent(TextView... view) {
		// TODO Auto-generated method stub

		Typeface type = Typeface.createFromAsset(getAssets(),
				Fonts.ROBOTO_MEDIUM.getPath());

		for (TextView v : view) {
			v.setTypeface(type);
		}
	}

	public void login(View v) {

		String login = loginText.getText().toString().trim();
		String password = passwordText.getText().toString().trim();

		if (TextUtils.isEmpty(login) || login.length() < 4) {
			loginText.requestFocus();
			loginText.setError(ErrorUtil.notValid("Nombre de usuario"));

		} else if (TextUtils.isEmpty(password) || password.length() < 8) {
			passwordText.requestFocus();
			passwordText.setError(ErrorUtil.notValid("Contrase�a"));

		} else {
			LoginReq request = new LoginReq().setLogin(login).setPassword(
					password);

			new WSClient(InicioActivity.this).hasLoader(true)
					.setCifrado(Cifrados.MOBILECARD)
					.setKey(request.getPassword())
					.setListener(new LoginListener()).setUrl(Url.LOGIN)
					.execute(request.buildLoginRequest(InicioActivity.this));
		}

	}
	
	public void goToRegistro(View v) {
		startActivity(new Intent(InicioActivity.this, RegistroActivity.class));
	}
	
	public void reenviar(View v) {
		Dialogs.enviarDialog(InicioActivity.this, new ReenviarListener());
	}
	
	public void recuperar(View v) {
		Dialogs.recuperarDialog(InicioActivity.this, new RecuperarListener());
	}

	private class LoginListener implements WSResponseListener {

		private static final String TAG = "LoginListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(InicioActivity.this,
					ErrorUtil.notValid("Nombre de usuario"), Toast.LENGTH_SHORT)
					.show();
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			int resultado = response.optInt("resultado", 10000);
			String mensaje = response.optString("mensaje", ErrorUtil.connection());
			String menArr[];
			Intent intent;
			
			if (1 == resultado) {
				
				menArr = mensaje.split("\\|");
				
				session.createLoginSession(
						menArr[1], 
						loginText.getText().toString().trim(), 
						passwordText.getText().toString().trim(), "");
				
				Toast.makeText(InicioActivity.this, menArr[0], Toast.LENGTH_LONG).show();
				
				intent = new Intent(InicioActivity.this, MenuActivity.class);
				startActivity(intent);
				
			} else if (98 == resultado || 99 == resultado) {
				
				menArr = mensaje.split("\\|");
				
				PasswordReq passReq = new PasswordReq()
							.setId(Long.parseLong(menArr[1]))
							.setLogin(loginText.getText().toString().trim())
							.setStatus(resultado);
				
				intent = new Intent(InicioActivity.this, PasswordActivity.class);
				intent.putExtra("request", passReq);
				startActivity(intent);
				
				Toast.makeText(InicioActivity.this, menArr[0], Toast.LENGTH_LONG).show();
				
			} else {
				Toast.makeText(InicioActivity.this, mensaje, Toast.LENGTH_SHORT).show();
			}
			
			
			
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(InicioActivity.this,
					ErrorUtil.notValid("Nombre de usuario"), Toast.LENGTH_SHORT)
					.show();

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(InicioActivity.this,
					ErrorUtil.notValid("Nombre de usuario"), Toast.LENGTH_SHORT)
					.show();

		}

	}
	
	private class RecuperarListener implements WSResponseListener {
		
		private static final String TAG = "RecuperarListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Log.d(TAG, response);
			if (TextUtils.equals("0", response)) {
				Toast.makeText(InicioActivity.this, "Contrase�a recuperada con �xito.", Toast.LENGTH_SHORT).show();
				
			} else {

				Toast.makeText(InicioActivity.this, "Error al recuperar contrase�a. Intente de nuevo m�s tarde.", Toast.LENGTH_SHORT).show();
			}
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Log.d(TAG, response.toString());
			Toast.makeText(InicioActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Log.d(TAG, response.toString());
			Toast.makeText(InicioActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Log.d(TAG, catalog.toString());
			Toast.makeText(InicioActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
		}
	}
	
	private class ReenviarListener implements WSResponseListener {
		
		private static final String TAG = "ReenviarListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Log.d(TAG, response);
			Toast.makeText(InicioActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			
			String mensaje = response.optString("mensaje", ErrorUtil.connection());
			Toast.makeText(InicioActivity.this, mensaje, Toast.LENGTH_SHORT).show();

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Log.d(TAG, response.toString());
			Toast.makeText(InicioActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Log.d(TAG, catalog.toString());
			Toast.makeText(InicioActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
		}
		
		
	}

}
