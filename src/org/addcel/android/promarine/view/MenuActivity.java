package org.addcel.android.promarine.view;

import org.addcel.android.font.Fonts;
import org.addcel.android.interfaces.Versionable;
import org.addcel.android.mobilecard.view.DatosActivity;
import org.addcel.android.promarine.R;
import org.addcel.android.session.SessionManager;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;

public class MenuActivity extends SherlockActivity implements Versionable {
	
	TextView comprarButton, consultarButton, actualizarButton, logoutButton;
	SessionManager session;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		session = new SessionManager(MenuActivity.this);
		
		initGUI();
	}

	private void initGUI() {
		comprarButton = (TextView) findViewById(R.id.button_comprar);
		consultarButton = (TextView) findViewById(R.id.button_consultar);
		actualizarButton = (TextView) findViewById(R.id.button_actualizar);
		
		logoutButton = (TextView) findViewById(R.id.button_session);
		
		int version = android.os.Build.VERSION.SDK_INT;
		
		if (14 > version)
			setLowerVersionContent(logoutButton);
	}
	
	@Override
	public void setLowerVersionContent(TextView... view) {
		// TODO Auto-generated method stub
		Typeface t = Typeface.createFromAsset(getAssets(), Fonts.ROBOTO_MEDIUM.getPath());
		
		for (TextView textView : view) {
			textView.setTypeface(t);
		}
	}
	
	public void comprar(View v) {
		
	}

	public void consultar(View v) {
		
	}
	
	public void actualizar(View v) {
		startActivity(new Intent(MenuActivity.this, DatosActivity.class));
	}
	
	public void logout(View v) {
		onBackPressed();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		session.logoutUser();
	}
}
