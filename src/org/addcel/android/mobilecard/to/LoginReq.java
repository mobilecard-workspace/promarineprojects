package org.addcel.android.mobilecard.to;

import org.addcel.android.crypto.Crypto;
import org.addcel.android.interfaces.JSONable;
import org.addcel.android.util.DeviceUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

public class LoginReq implements JSONable, Parcelable {
	
	private String login;
	private String password;
	
	public LoginReq() {
		
	}
	
	public LoginReq(Parcel source) {
		readFromParcel(source);
	}
	
	public LoginReq(JSONObject json) {
		fromJSON(json);
	}
	
	
	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public LoginReq setLogin(String login) {
		this.login = login;
		return this;
	}

	public LoginReq setPassword(String password) {
		this.password = password;
		return this;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(login);
		dest.writeString(password);
	}
	
	public void readFromParcel(Parcel source) {
		login = source.readString();
		password = source.readString();
	}

	@Override
	public JSONObject toJSON() {
		// TODO Auto-generated method stub
		try {
			return new JSONObject().put("login", login)
			.put("password", password);
		} catch (JSONException e) {
			return null;
		}
	}

	@Override
	public void fromJSON(JSONObject json) {
		// TODO Auto-generated method stub
		login = json.optString("login");
		password = json.optString("password");
	}
	
	public String buildLoginRequest(Context con) {
		
		try {
			
			return toJSON().put("imei", DeviceUtils.getIMEI(con))
							.put("passwordS", Crypto.sha1(password))
							.put("tipo", DeviceUtils.getTipo())
							.put("software", DeviceUtils.getSWVersion())
							.put("modelo", DeviceUtils.getModel())
							.put("key", DeviceUtils.getIMEI(con))
							.toString();
			
		} catch (JSONException e) {
			return null;
		}
		
	}
	
	public static final Parcelable.Creator<LoginReq> CREATOR = new Creator<LoginReq>() {
		
		@Override
		public LoginReq[] newArray(int size) {
			// TODO Auto-generated method stub
			return new LoginReq[size];
		}
		
		@Override
		public LoginReq createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new LoginReq(source);
		}
	};
	

}
