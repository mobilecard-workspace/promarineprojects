
package org.addcel.android.mobilecard.enums;

public enum Sexo {

	MASCULINO("M", "Masculino", 0), FEMENINO("F", "Femenino", 1);
	
	String clave;
	String descripcion;
	int index;
	
	private Sexo(String _clave, String _descripcion, int _index) {
		clave = _clave;
		descripcion = _descripcion;
		index = _index;
	}
	
	public String getClave() {
		return clave;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public int getIndex() {
		return index;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return descripcion;
	}
}
