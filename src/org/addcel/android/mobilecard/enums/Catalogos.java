package org.addcel.android.mobilecard.enums;

public enum Catalogos {

	PROVEEDOR("proveedores"), ESTADO("estados"), TIPO_TARJETA("tarjetas");
	
	private String nombre;
	
	private Catalogos(String _nombre) {
		nombre = _nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
}
