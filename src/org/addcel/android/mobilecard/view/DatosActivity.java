package org.addcel.android.mobilecard.view;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.addcel.android.crypto.Cifrados;
import org.addcel.android.font.Fonts;
import org.addcel.android.interfaces.Versionable;
import org.addcel.android.mobilecard.constant.Url;
import org.addcel.android.mobilecard.enums.Catalogos;
import org.addcel.android.mobilecard.enums.Meses;
import org.addcel.android.mobilecard.enums.Sexo;
import org.addcel.android.mobilecard.to.PasswordReq;
import org.addcel.android.mobilecard.to.Usuario;
import org.addcel.android.promarine.R;
import org.addcel.android.promarine.view.InicioActivity;
import org.addcel.android.session.SessionManager;
import org.addcel.android.util.AppUtils;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.util.GraphicUtil;
import org.addcel.android.util.Text;
import org.addcel.android.util.Validador;
import org.addcel.android.widget.SpinnerValues;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class DatosActivity extends SherlockActivity implements Versionable {

	TextView headerView, loginView, celularView, confCelView, proveedorView, mailView,
			confMailView, nombreView, paternoView, maternoView, nacimientoView,
			sexoView, telCasaView, telOficinaView, estadoView, ciudadView,
			calleView, numExtView, numIntView, coloniaView, cpView, tarjetaView,
			tipoTarjetaView, vigenciaView, terminosButton, cancelarButton,
			registrarButton;

	EditText loginText, celularText, confCelText, mailText, confMailText,
			nombreText, paternoText, maternoText, nacimientoText, telCasaText,
			telOficinaText, ciudadText, calleText, numExtText, numIntText,
			coloniaText, cpText, tarjetaText;

	Spinner proveedorSpinner, sexoSpinner, estadoSpinner, tipoTarjetaSpinner,
			mesSpinner, anioSpinner;

	List<BasicNameValuePair> proveedores, estados, tipoTarjeta, sexos;

	SpinnerValues proveedorValues, estadoValues, tipoTarjetaValues;
	SessionManager session;

	private DatePickerDialog nacimientoDialog;
	private int anio, mes, dia;
	private Date nacimientoDate;

	Usuario usuario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);

		session = new SessionManager(DatosActivity.this);

		initGUI();
		
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;

		if (14 > currentapiVersion)

			setLowerVersionContent(loginView, celularView, confCelView,
					proveedorView, mailView, confMailView, nombreView,
					paternoView, maternoView, nacimientoView, sexoView,
					telCasaView, telOficinaView, estadoView, ciudadView,
					calleView, numExtView, numIntView, coloniaView, cpView,
					tarjetaView, tipoTarjetaView, vigenciaView, terminosButton,
					cancelarButton, registrarButton, loginText, celularText,
					confCelText, mailText, confMailText, nombreText,
					paternoText, maternoText, nacimientoText, telCasaText,
					telOficinaText, ciudadText, calleText, numExtText,
					numIntText, coloniaText, cpText, tarjetaText);


		new WSClient(DatosActivity.this).setCatalogo(Catalogos.PROVEEDOR)
				.setCifrado(Cifrados.HARD).setListener(new ProveedorListener())
				.setUrl(Url.PROVIDERS_GET).execute();

	}

	@Override
	public void setLowerVersionContent(TextView... view) {
		// TODO Auto-generated method stub
		Typeface t = Typeface.createFromAsset(getAssets(),
				Fonts.ROBOTO_REGULAR.getPath());

		for (TextView v : view) {
			v.setTypeface(t);
		}
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public Date getNacimientoDate() {
		return nacimientoDate;
	}

	public void setNacimientoDate(Date nacimientoDate) {
		this.nacimientoDate = nacimientoDate;
	}

	public DatePickerDialog getNacimientoDialog() {
		if (nacimientoDialog == null) {

			final Calendar c = Calendar.getInstance();

			nacimientoDialog = new DatePickerDialog(DatosActivity.this,
					new OnDateSetListener() {

						@Override
						public void onDateSet(DatePicker view, int year,
								int monthOfYear, int dayOfMonth) {
							// TODO Auto-generated method stub
							dia = dayOfMonth;
							mes = monthOfYear;
							anio = year;

							c.set(anio, mes, dia);

							setNacimientoDate(c.getTime());
							nacimientoText.setText(Text
									.formatDateForShowing(getNacimientoDate()));

						}
					}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c
							.get(Calendar.DAY_OF_MONTH));

			int currentapiVersion = android.os.Build.VERSION.SDK_INT;

			if (currentapiVersion > 10) {
				nacimientoDialog.getDatePicker().setCalendarViewShown(false);
			}
		}

		return nacimientoDialog;
	}

	private void initGUI() {
		
		headerView = (TextView) findViewById(R.id.header);
		
		GraphicUtil.setBackgroundDrawableWithBuildValidation(headerView, getResources().getDrawable(R.drawable.h_actualizardatos));
		
		findViewById(R.id.button_password).setVisibility(View.VISIBLE);

		loginView = (TextView) findViewById(R.id.view_user);
		celularView = (TextView) findViewById(R.id.view_celular);
		confCelView = (TextView) findViewById(R.id.view_confcel);
		proveedorView = (TextView) findViewById(R.id.view_proveedor);
		mailView = (TextView) findViewById(R.id.view_email);
		confMailView = (TextView) findViewById(R.id.view_confmail);
		nombreView = (TextView) findViewById(R.id.view_nombres);
		paternoView = (TextView) findViewById(R.id.view_apellido);
		maternoView = (TextView) findViewById(R.id.view_materno);
		nacimientoView = (TextView) findViewById(R.id.view_nacimiento);
		sexoView = (TextView) findViewById(R.id.view_sexo);
		telCasaView = (TextView) findViewById(R.id.view_casa);
		telOficinaView = (TextView) findViewById(R.id.view_oficina);
		estadoView = (TextView) findViewById(R.id.view_estado);
		ciudadView = (TextView) findViewById(R.id.view_ciudad);
		calleView = (TextView) findViewById(R.id.view_calle);
		numExtView = (TextView) findViewById(R.id.view_exterior);
		numIntView = (TextView) findViewById(R.id.view_interior);
		coloniaView = (TextView) findViewById(R.id.view_colonia);
		cpView = (TextView) findViewById(R.id.view_cp);
		tarjetaView = (TextView) findViewById(R.id.view_tarjeta);
		tipoTarjetaView = (TextView) findViewById(R.id.view_tipo);
		vigenciaView = (TextView) findViewById(R.id.view_vigencia);
		
		terminosButton = (TextView) findViewById(R.id.button_terminos);
		terminosButton.setVisibility(View.GONE);
		
		cancelarButton = (TextView) findViewById(R.id.button_cancelar);
		
		registrarButton = (TextView) findViewById(R.id.button_registrar);
		GraphicUtil.setBackgroundDrawableWithBuildValidation(registrarButton, getResources().getDrawable(R.drawable.s_actualizar));

		loginText = (EditText) findViewById(R.id.text_user);
		celularText = (EditText) findViewById(R.id.text_celular);
		confCelText = (EditText) findViewById(R.id.text_confcel);

		mailText = (EditText) findViewById(R.id.text_email);
		confMailText = (EditText) findViewById(R.id.text_confmail);
		nombreText = (EditText) findViewById(R.id.text_nombres);
		paternoText = (EditText) findViewById(R.id.text_apellido);
		maternoText = (EditText) findViewById(R.id.text_materno);
		nacimientoText = (EditText) findViewById(R.id.text_nacimiento);
		telCasaText = (EditText) findViewById(R.id.text_casa);
		telOficinaText = (EditText) findViewById(R.id.text_oficina);

		ciudadText = (EditText) findViewById(R.id.text_ciudad);
		calleText = (EditText) findViewById(R.id.text_calle);
		numExtText = (EditText) findViewById(R.id.text_exterior);
		numIntText = (EditText) findViewById(R.id.text_interior);
		coloniaText = (EditText) findViewById(R.id.text_colonia);
		cpText = (EditText) findViewById(R.id.text_cp);
		tarjetaText = (EditText) findViewById(R.id.text_tarjeta);

		proveedorSpinner = (Spinner) findViewById(R.id.spinner_proveedor);
		sexoSpinner = (Spinner) findViewById(R.id.spinner_sexo);
		estadoSpinner = (Spinner) findViewById(R.id.spinner_estado);
		tipoTarjetaSpinner = (Spinner) findViewById(R.id.spinner_tipo);
		mesSpinner = (Spinner) findViewById(R.id.spinner_mes);
		anioSpinner = (Spinner) findViewById(R.id.spinner_anio);

	}
	
	public void showDate(View v) {
		if (getNacimientoDialog().isShowing()) {
			getNacimientoDialog().dismiss();
		}
		
		getNacimientoDialog().show();
	}
	
	private boolean validate() {
		
		String nombre = nombreText.getText().toString().trim();
		String paterno = paternoText.getText().toString().trim();
		String nacimiento = nacimientoText.getText().toString().trim();
		String ciudad = ciudadText.getText().toString().trim();
		String calle = calleText.getText().toString().trim();
		String numExt = numExtText.getText().toString().trim();
		String colonia = coloniaText.getText().toString().trim();
		String cp = cpText.getText().toString().trim();
		String tarjeta = tarjetaText.getText().toString().trim();
		
		if (TextUtils.isEmpty(nombre)) {
			nombreText.requestFocus();
			nombreText.setError(ErrorUtil.isEmpty("Nombre(s)"));
			nombreText.setText(usuario.getNombre());
			return false;
		}
		
		if (nombre.length() < 2) {
			nombreText.requestFocus();
			nombreText.setError(ErrorUtil.notValid("Nombre"));
			nombreText.setText(usuario.getNombre());
			return false;
		}
		
		if (TextUtils.isEmpty(paterno)) {
			paternoText.requestFocus();
			paternoText.setError(ErrorUtil.isEmpty("Apellido paterno"));
			paternoText.setText(usuario.getApellido());
			return false;
		}
		
		if (paterno.length() < 2) {
			paternoText.requestFocus();
			paternoText.setText(ErrorUtil.notValid("Apellido paterno"));
			paternoText.setText(usuario.getApellido());
			return false;
		}
		
		if (TextUtils.isEmpty(nacimiento) || getNacimientoDate() == null) {
			nacimientoText.requestFocus();
			nacimientoText.setError("Selecciona tu fecha de nacimiento");
			return false;
		}
		
		if (TextUtils.isEmpty(ciudad)) {
			ciudadText.requestFocus();
			ciudadText.setError(ErrorUtil.isEmpty("Calle"));
			ciudadText.setText(usuario.getCiudad());
			return false;
		}
		
		if (TextUtils.isEmpty(calle)) {
			calleText.requestFocus();
			calleText.setError(ErrorUtil.isEmpty("Calle"));
			calleText.setText(usuario.getCalle());
			return false;
		}
		
		if (TextUtils.isEmpty(numExt)) {
			numExtText.requestFocus();
			numExtText.setError(ErrorUtil.isEmpty("N�mero exterior"));
			numExtText.setText(String.valueOf(usuario.getNumExt()));
			return false;
		}
		
		if (!TextUtils.isDigitsOnly(numExt) || numExt.length() > 5) {
			numExtText.requestFocus();
			numExtText.setError(ErrorUtil.notValid("N�mero exterior"));
			numExtText.setText(usuario.getNumExt());
			return false;
		}
		
		if (TextUtils.isEmpty(colonia)) {
			coloniaText.requestFocus();
			coloniaText.setError(ErrorUtil.isEmpty("Colonia"));
			coloniaText.setText(usuario.getColonia());
			return false;
		}
		
		if (TextUtils.isEmpty(cp)) {
			cpText.requestFocus();
			cpText.setError(ErrorUtil.isEmpty("C�digo Postal"));
			cpText.setText(usuario.getCp());
			return false;
		}
		
		if (TextUtils.isEmpty(tarjeta)) {
			tarjetaText.requestFocus();
			tarjetaText.setError(ErrorUtil.isEmpty("N�mero de tarjeta"));
			tarjetaText.setText(usuario.getMaskedTarjeta());
			return false;
		}
		
		if (!TextUtils.equals(usuario.getMaskedTarjeta(), tarjeta) && !TextUtils.isDigitsOnly(tarjeta)) {
			tarjetaText.requestFocus();
			tarjetaText.setError("Ingresa tu nuevo n�mero de tarjeta");
			tarjetaText.setText(usuario.getMaskedTarjeta());
			
			return false;
		}
		
		if (TextUtils.isDigitsOnly(tarjeta) && tipoTarjetaValues.getValorSeleccionado().equals("1") && !Validador.esTarjetaDeCreditoVisa(tarjeta)) {
			tarjetaText.requestFocus();
			tarjetaText.setError(ErrorUtil.notValid("N�mero de tarjeta"));
			tarjetaText.setText(usuario.getMaskedTarjeta());
			return false;
		}
		
		if (TextUtils.isDigitsOnly(tarjeta) && tipoTarjetaValues.getValorSeleccionado().equals("2") && !Validador.esTarjetaDeCreditoMasterCard(tarjeta)) {
			tarjetaText.requestFocus();
			tarjetaText.setError(ErrorUtil.notValid("N�mero de tarjeta"));
			tarjetaText.setText(usuario.getMaskedTarjeta());
			return false;
		}
		
		return true;
	}
	
	public void updatePass(View v) {

		PasswordReq passReq = new PasswordReq()
					.setId(usuario.getUsuario())
					.setLogin(usuario.getLogin())
					.setStatus(1);
		
		Intent intent = new Intent(DatosActivity.this, PasswordActivity.class);
		intent.putExtra("request", passReq);
		startActivity(intent);
		
	}

	public void registrar(View v) {
		
		if (validate()) {

			String vigencia = ((Meses) mesSpinner.getSelectedItem()).getClave()
					+ "/"
					+ ((String) anioSpinner.getSelectedItem()).substring(2, 4);
			
			String tarjeta = tarjetaText.getText().toString().trim();
			
			if (!TextUtils.equals(tarjeta, usuario.getMaskedTarjeta()))
				usuario.setTarjeta(tarjeta);
			
			usuario.setApellido(paternoText.getText().toString().trim())
					.setCalle(calleText.getText().toString().trim())
					.setCiudad(ciudadText.getText().toString().trim())
					.setColonia(coloniaText.getText().toString().trim())
					.setCp(cpText.getText().toString().trim())
					.setIdEstado(
							Integer.parseInt(estadoValues.getValorSeleccionado()))
					.setMaterno(maternoText.getText().toString().trim())
					.setNacimiento(Text.formatDate(getNacimientoDate()))
					.setNombre(nombreText.getText().toString().trim())
					.setNumExt(
							Integer.parseInt(numExtText.getText().toString().trim()))
					.setNumInterior(numIntText.getText().toString().trim())
					.setPassword(
							session.getUserDetails().get(SessionManager.USR_PWD))
					.setProveedor(
							Integer.parseInt(proveedorValues.getValorSeleccionado()))
					.setSexo((Sexo) sexoSpinner.getSelectedItem())
					.setTelCasa(telCasaText.getText().toString().trim())
					.setTelOficina(telOficinaText.getText().toString().trim())
					.setTipoTarjeta(
							Integer.parseInt(tipoTarjetaValues
									.getValorSeleccionado())).setVigencia(vigencia);
	
			new WSClient(DatosActivity.this).hasLoader(true).setCancellable(false)
					.setCifrado(Cifrados.MOBILECARD).setKey(usuario.getPassword())
					.setListener(new SetDatosListener()).setResponseTimeout(60000)
					.setUrl(Url.UPDATE).execute(usuario.toJSON().toString());
		}
	}

	private class ProveedorListener implements WSResponseListener {

		private static final String TAG = "ProveedorListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			proveedores = catalog;
			proveedorValues = new SpinnerValues(DatosActivity.this,
					proveedorSpinner, proveedores);

			new WSClient(DatosActivity.this).setCatalogo(Catalogos.ESTADO)
					.setCancellable(false).setCifrado(Cifrados.HARD)
					.setListener(new EstadosListener()).setUrl(Url.ESTADOS_GET)
					.execute();
		}

	}

	private class EstadosListener implements WSResponseListener {

		private static final String TAG = "EstadosListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			estados = catalog;
			estadoValues = new SpinnerValues(DatosActivity.this, estadoSpinner,
					estados);

			new WSClient(DatosActivity.this).setCancellable(false)
					.setCatalogo(Catalogos.TIPO_TARJETA)
					.setCifrado(Cifrados.HARD)
					.setListener(new TipoTarjetaListener())
					.setUrl(Url.CARD_TYPE_GET).execute();
		}

	}

	private class TipoTarjetaListener implements WSResponseListener {

		private static final String TAG = "TipoTarjetaListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			tipoTarjeta = catalog;
			tipoTarjeta.remove(2);

			tipoTarjetaValues = new SpinnerValues(DatosActivity.this,
					tipoTarjetaSpinner, tipoTarjeta);

			ArrayAdapter<Sexo> adapter = new ArrayAdapter<Sexo>(
					DatosActivity.this, R.layout.sherlock_spinner_item,
					Sexo.values());
			adapter.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);

			sexoSpinner.setAdapter(adapter);

			ArrayAdapter<Meses> mesAdapter = new ArrayAdapter<Meses>(
					DatosActivity.this, R.layout.sherlock_spinner_item,
					Meses.values());

			mesAdapter
					.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);

			mesSpinner.setAdapter(mesAdapter);

			ArrayAdapter<String> anioAdapter = AppUtils
					.getAniosVigenciaSAdapter(DatosActivity.this);
			anioAdapter
					.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);

			anioSpinner.setAdapter(anioAdapter);

			try {

				String request = new JSONObject()
						.put("login",
								session.getUserDetails().get(
										SessionManager.USR_LOGIN))
						.put("password",
								session.getUserDetails().get(
										SessionManager.USR_PWD)).toString();

				new WSClient(DatosActivity.this)
						.setCancellable(false)
						.setCifrado(Cifrados.MOBILECARD)
						.setKey(session.getUserDetails().get(
								SessionManager.USR_PWD))
						.setListener(new GetDatosListener())
						.setUrl(Url.USER_GET).execute(request);

			} catch (JSONException e) {

			}
		}
	}

	private class GetDatosListener implements WSResponseListener {

		private static final String TAG = "GetDatosListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Log.d(TAG, response.toString());
			usuario = new Usuario(response);

			loginText.setText(usuario.getLogin());
			loginText.setEnabled(false);
			celularText.setText(usuario.getTelefono());
			celularText.setEnabled(false);

			confCelView.setVisibility(View.GONE);
			confCelText.setVisibility(View.GONE);

			mailText.setText(usuario.getMail());
			mailText.setEnabled(false);

			confMailView.setVisibility(View.GONE);
			confMailText.setVisibility(View.GONE);

			nombreText.setText(usuario.getNombre());
			paternoText.setText(usuario.getApellido());
			maternoText.setText(usuario.getMaterno());
			nacimientoText.setText(usuario.getNacimiento());
			setNacimientoDate(Text.getSimpleDateFromString(usuario.getNacimiento()));
			
			telCasaText.setText(usuario.getTelCasa());
			telOficinaText.setText(usuario.getTelOficina());

			ciudadText.setText(usuario.getCiudad());
			calleText.setText(usuario.getCalle());
			numExtText.setText(String.valueOf(usuario.getNumExt()));
			numIntText.setText(usuario.getNumInterior());
			coloniaText.setText(usuario.getColonia());
			cpText.setText(usuario.getCp());
			tarjetaText.setText(usuario.getMaskedTarjeta());

			proveedorValues.setValorSeleccionado(String.valueOf(usuario
					.getProveedor()));
			estadoValues.setValorSeleccionado(String.valueOf(usuario
					.getIdEstado()));
			tipoTarjetaValues.setValorSeleccionado(String.valueOf(usuario
					.getTipoTarjeta()));

			sexoSpinner.setSelection(usuario.getSexo().getIndex());

			String[] vigenciaArr = usuario.getVigencia().split("\\/");

			mesSpinner.setSelection(Meses.pickMes(vigenciaArr[0]).getIndex());
			anioSpinner.setSelection(AppUtils
					.getAnioVigenciaPosition(vigenciaArr[1]));

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_LONG).show();
			finish();

		}

	}

	private class SetDatosListener implements WSResponseListener {

		private static final String TAG = "SetDatosListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_SHORT).show();

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub

			int resultado = response.optInt("resultado", 10000);
			String mensaje = response.optString("mensaje",
					ErrorUtil.connection());

			switch (resultado) {
			case 1:
				Toast.makeText(DatosActivity.this, mensaje, Toast.LENGTH_LONG)
						.show();
				finish();
				break;

			default:
				Toast.makeText(DatosActivity.this, mensaje, Toast.LENGTH_SHORT)
						.show();
				break;
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_SHORT).show();

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(DatosActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_SHORT).show();

		}

	}

}
