package org.addcel.android.mobilecard.view;

import org.addcel.android.font.Fonts;
import org.addcel.android.interfaces.Versionable;
import org.addcel.android.promarine.R;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;

public class TerminosActivity extends SherlockActivity implements Versionable {
	
	TextView terminosView;
	String terminos;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_terminos);
		
		terminos = getIntent().getStringExtra("terminos");
		terminosView = (TextView) findViewById(R.id.view_terminos);
		
		int version = android.os.Build.VERSION.SDK_INT;
		
		if (14 > version)
			setLowerVersionContent(terminosView);
		
		terminosView.setText(terminos);
			
	}

	@Override
	public void setLowerVersionContent(TextView... view) {
		
		Typeface t = Typeface.createFromAsset(getAssets(), Fonts.ROBOTO_REGULAR.getPath());
		
		// TODO Auto-generated method stub
		for (TextView textView : view) {
			textView.setTypeface(t);
		}
	}
	
	public void cancelar(View v) {
		setResult(2);
		finish();
	}
	
	public void aceptar(View v) {
		setResult(RESULT_OK);
		finish();
	}

}
