package org.addcel.android.mobilecard.view;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.addcel.android.crypto.AddcelCrypto;
import org.addcel.android.crypto.Cifrados;
import org.addcel.android.font.Fonts;
import org.addcel.android.interfaces.Versionable;
import org.addcel.android.mobilecard.constant.Url;
import org.addcel.android.mobilecard.enums.Catalogos;
import org.addcel.android.mobilecard.enums.Meses;
import org.addcel.android.mobilecard.enums.Sexo;
import org.addcel.android.mobilecard.to.Usuario;
import org.addcel.android.promarine.R;
import org.addcel.android.util.AppUtils;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.util.GraphicUtil;
import org.addcel.android.util.Text;
import org.addcel.android.widget.SpinnerValues;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class RegistroActivity extends SherlockActivity implements Versionable {

	TextView loginView, celularView, confCelView, proveedorView, mailView,
			confMailView, nombreView, paternoView, maternoView, nacimientoView,
			sexoView, telCasaView, telOficinaView, estadoView, ciudadView,
			calleView, numExtView, numIntView, coloniaView, tarjetaView,
			tipoTarjetaView, vigenciaView, terminosButton, cancelarButton,
			registrarButton;

	EditText loginText, celularText, confCelText, mailText, confMailText,
			nombreText, paternoText, maternoText, nacimientoText, telCasaText,
			telOficinaText, ciudadText, calleText, numExtText, numIntText,
			coloniaText, tarjetaText;

	Spinner proveedorSpinner, sexoSpinner, estadoSpinner, tipoTarjetaSpinner,
			mesSpinner, anioSpinner;

	SpinnerValues proveedorValues, sexoValues, estadoValues, tipoTarjetaValues;

	List<BasicNameValuePair> proveedores, estados, tipoTarjeta, sexos;
	
	private DatePickerDialog nacimientoDialog;
	private int anio, mes, dia;
	private Date nacimientoDate;

	private static final int GET_TERMINOS = 2;

	private String terminos;
	private boolean hasTerminos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registro);

		initGUI();

		int currentapiVersion = android.os.Build.VERSION.SDK_INT;

		if (14 > currentapiVersion)

			setLowerVersionContent(loginView, celularView, confCelView,
					proveedorView, mailView, confMailView, nombreView,
					paternoView, maternoView, nacimientoView, sexoView,
					telCasaView, telOficinaView, estadoView, ciudadView,
					calleView, numExtView, numIntView, coloniaView,
					tarjetaView, tipoTarjetaView, vigenciaView, terminosButton,
					cancelarButton, registrarButton, loginText, celularText,
					confCelText, mailText, confMailText, nombreText,
					paternoText, maternoText, nacimientoText, telCasaText,
					telOficinaText, ciudadText, calleText, numExtText,
					numIntText, coloniaText, tarjetaText);

		new WSClient(RegistroActivity.this).setCatalogo(Catalogos.PROVEEDOR)
				.setCifrado(Cifrados.HARD).setListener(new ProveedorListener())
				.setUrl(Url.PROVIDERS_GET).execute();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == GET_TERMINOS) {

			findViewById(R.id.button_terminos).requestFocus();

			switch (resultCode) {
			case RESULT_OK:
				hasTerminos = true;

				GraphicUtil.setBackgroundDrawableWithBuildValidation(findViewById(R.id.button_terminos), 
						getResources().getDrawable(R.drawable.s_terminos_on));
				break;

			default:
				hasTerminos = false;
				GraphicUtil.setBackgroundDrawableWithBuildValidation(findViewById(R.id.button_terminos), 
						getResources().getDrawable(R.drawable.s_terminos));

				break;
			}

		}
	}
	
	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public Date getNacimientoDate() {
		return nacimientoDate;
	}

	public void setNacimientoDate(Date nacimientoDate) {
		this.nacimientoDate = nacimientoDate;
	}
	
	public DatePickerDialog getNacimientoDialog() {
		if (nacimientoDialog == null ) {
			
			final Calendar c = Calendar.getInstance();
			
			nacimientoDialog = new DatePickerDialog(RegistroActivity.this, new OnDateSetListener() {
				
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear,
						int dayOfMonth) {
					// TODO Auto-generated method stub
					dia = dayOfMonth;
					mes = monthOfYear;
					anio = year;
					
					c.set(anio, mes, dia);
					
					setNacimientoDate(c.getTime());
					nacimientoText.setText(Text.formatDateForShowing(getNacimientoDate()));
					
				}
			}, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
			
			int currentapiVersion = android.os.Build.VERSION.SDK_INT;

			if (currentapiVersion > 10) {
				nacimientoDialog.getDatePicker().setCalendarViewShown(false);
			}
		} 
		
		return nacimientoDialog;
	}

	private void initGUI() {
		loginView = (TextView) findViewById(R.id.view_user);
		celularView = (TextView) findViewById(R.id.view_celular);
		confCelView = (TextView) findViewById(R.id.view_confcel);
		proveedorView = (TextView) findViewById(R.id.view_proveedor);
		mailView = (TextView) findViewById(R.id.view_email);
		confMailView = (TextView) findViewById(R.id.view_confmail);
		nombreView = (TextView) findViewById(R.id.view_nombres);
		paternoView = (TextView) findViewById(R.id.view_apellido);
		maternoView = (TextView) findViewById(R.id.view_materno);
		nacimientoView = (TextView) findViewById(R.id.view_nacimiento);
		sexoView = (TextView) findViewById(R.id.view_sexo);
		telCasaView = (TextView) findViewById(R.id.view_casa);
		telOficinaView = (TextView) findViewById(R.id.view_oficina);
		estadoView = (TextView) findViewById(R.id.view_estado);
		ciudadView = (TextView) findViewById(R.id.view_ciudad);
		calleView = (TextView) findViewById(R.id.view_calle);
		numExtView = (TextView) findViewById(R.id.view_exterior);
		numIntView = (TextView) findViewById(R.id.view_interior);
		coloniaView = (TextView) findViewById(R.id.view_colonia);
		tarjetaView = (TextView) findViewById(R.id.view_tarjeta);
		tipoTarjetaView = (TextView) findViewById(R.id.view_tipo);
		vigenciaView = (TextView) findViewById(R.id.view_vigencia);
		terminosButton = (TextView) findViewById(R.id.button_terminos);
		cancelarButton = (TextView) findViewById(R.id.button_cancelar);
		registrarButton = (TextView) findViewById(R.id.button_registrar);

		loginText = (EditText) findViewById(R.id.text_user);
		celularText = (EditText) findViewById(R.id.text_celular);
		confCelText = (EditText) findViewById(R.id.text_confcel);

		mailText = (EditText) findViewById(R.id.text_email);
		confMailText = (EditText) findViewById(R.id.text_confmail);
		nombreText = (EditText) findViewById(R.id.text_nombres);
		paternoText = (EditText) findViewById(R.id.text_apellido);
		maternoText = (EditText) findViewById(R.id.text_materno);
		nacimientoText = (EditText) findViewById(R.id.text_nacimiento);

		telCasaText = (EditText) findViewById(R.id.text_casa);
		telOficinaText = (EditText) findViewById(R.id.text_oficina);

		ciudadText = (EditText) findViewById(R.id.text_ciudad);
		calleText = (EditText) findViewById(R.id.text_calle);
		numExtText = (EditText) findViewById(R.id.text_exterior);
		numIntText = (EditText) findViewById(R.id.text_interior);
		coloniaText = (EditText) findViewById(R.id.text_colonia);
		tarjetaText = (EditText) findViewById(R.id.text_tarjeta);

		proveedorSpinner = (Spinner) findViewById(R.id.spinner_proveedor);
		sexoSpinner = (Spinner) findViewById(R.id.spinner_sexo);
		estadoSpinner = (Spinner) findViewById(R.id.spinner_estado);
		tipoTarjetaSpinner = (Spinner) findViewById(R.id.spinner_tipo);
		mesSpinner = (Spinner) findViewById(R.id.spinner_mes);
		anioSpinner = (Spinner) findViewById(R.id.spinner_anio);

	}

	@Override
	public void setLowerVersionContent(TextView... view) {
		// TODO Auto-generated method stub

		Typeface t = Typeface.createFromAsset(getAssets(),
				Fonts.ROBOTO_REGULAR.getPath());

		for (TextView v : view) {
			v.setTypeface(t);
		}
	}

	public void showDate(View v) {
		if (getNacimientoDialog().isShowing()) {
			getNacimientoDialog().dismiss();
		}
		
		getNacimientoDialog().show();
	}
	
	public void goToTerminos(View v) {

		if (TextUtils.isEmpty(terminos))

			new WSClient(RegistroActivity.this).hasLoader(true)
					.setCancellable(false).setCifrado(Cifrados.HARD)
					.setListener(new TerminosListener()).setUrl(Url.TERMINOS)
					.execute();

		else {

			Intent i = new Intent(RegistroActivity.this, TerminosActivity.class);
			i.putExtra("terminos", terminos);

			startActivityForResult(i, GET_TERMINOS);
		}

	}

	public void cancelar(View v) {
		onBackPressed();
	}

	public void registrar(View v) {

		Usuario user = new Usuario();

		user.setApellido(paternoText.getText().toString().trim())
				.setCalle(calleText.getText().toString().trim())
				.setCiudad(ciudadText.getText().toString().trim())
				.setColonia(coloniaText.getText().toString().trim())
				.setCp("")
				.setIdEstado(
						Integer.parseInt(estadoValues.getValorSeleccionado()))
				.setLogin(loginText.getText().toString().trim())
				.setMail(mailText.getText().toString().trim())
				.setMaterno(maternoText.getText().toString().trim())
				.setNacimiento(Text.formatDate(nacimientoDate))
				.setNombre(nombreText.getText().toString().trim())
				.setNumExt(
						Integer.parseInt(numExtText.getText().toString().trim()))
				.setNumInterior(numIntText.getText().toString().trim())
				.setPassword("")
				.setProveedor(
						Integer.parseInt(proveedorValues.getValorSeleccionado()))
				.setRegistro(Text.formatDate(Calendar.getInstance().getTime()))
				.setSexo(((Sexo) sexoSpinner.getSelectedItem()))
				.setTarjeta(tarjetaText.getText().toString().trim())
				.setTelCasa(telCasaText.getText().toString().trim())
				.setTelefono(celularText.getText().toString().trim())
				.setTelOficina(telOficinaText.getText().toString().trim())
				.setTipoTarjeta(
						Integer.parseInt(tipoTarjetaValues
								.getValorSeleccionado()))
				.setVigencia(
						((Meses) mesSpinner.getSelectedItem()).getClave()
								+ "/"
								+ ((String) anioSpinner.getSelectedItem())
										.substring(2, 4));

		new WSClient(RegistroActivity.this).hasLoader(true)
				.setCancellable(false).setCifrado(Cifrados.MOBILECARD)
				.setKey(AddcelCrypto.getKeyReg()).setListener(null)
				.setResponseTimeout(60000).setUrl(Url.INSERT)
				.execute(user.buildRegistroRequest(RegistroActivity.this));

	}

	private class ProveedorListener implements WSResponseListener {

		private static final String TAG = "ProveedorListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			proveedores = catalog;
			proveedorValues = new SpinnerValues(RegistroActivity.this,
					proveedorSpinner, proveedores);

			new WSClient(RegistroActivity.this).setCatalogo(Catalogos.ESTADO)
					.setCifrado(Cifrados.HARD)
					.setListener(new EstadosListener()).setUrl(Url.ESTADOS_GET)
					.execute();
		}

	}

	private class EstadosListener implements WSResponseListener {

		private static final String TAG = "EstadosListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			estados = catalog;
			estadoValues = new SpinnerValues(RegistroActivity.this,
					estadoSpinner, estados);

			new WSClient(RegistroActivity.this)
					.setCatalogo(Catalogos.TIPO_TARJETA)
					.setCifrado(Cifrados.HARD)
					.setListener(new TipoTarjetaListener())
					.setUrl(Url.CARD_TYPE_GET).execute();
		}

	}

	private class TipoTarjetaListener implements WSResponseListener {

		private static final String TAG = "TipoTarjetaListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			tipoTarjeta = catalog;
			tipoTarjetaValues = new SpinnerValues(RegistroActivity.this,
					tipoTarjetaSpinner, tipoTarjeta);

			ArrayAdapter<Sexo> adapter = new ArrayAdapter<Sexo>(
					RegistroActivity.this, R.layout.sherlock_spinner_item,
					Sexo.values());
			adapter.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);

			sexoSpinner.setAdapter(adapter);

			ArrayAdapter<Meses> mesAdapter = new ArrayAdapter<Meses>(
					RegistroActivity.this, R.layout.sherlock_spinner_item,
					Meses.values());

			mesAdapter
					.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);

			mesSpinner.setAdapter(mesAdapter);

			ArrayAdapter<String> anioAdapter = AppUtils
					.getAniosVigenciaSAdapter(RegistroActivity.this);
			anioAdapter
					.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
			
			anioSpinner.setAdapter(anioAdapter);

		}

	}

	private class TerminosListener implements WSResponseListener {

		private static final String TAG = "TerminosListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(RegistroActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			Log.d(TAG, response.toString());

			terminos = response.optString("Descripcion", "");

			if (!TextUtils.isEmpty(terminos)) {

				Intent i = new Intent(RegistroActivity.this,
						TerminosActivity.class);
				i.putExtra("terminos", terminos);

				startActivityForResult(i, GET_TERMINOS);

			} else {

				Toast.makeText(RegistroActivity.this,
						ErrorUtil.notAvailable("T�rminos"), Toast.LENGTH_SHORT)
						.show();
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(RegistroActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_SHORT).show();

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(RegistroActivity.this, ErrorUtil.connection(),
					Toast.LENGTH_SHORT).show();

		}

	}

	private class RegistroListener implements WSResponseListener {

		private static final String TAG = "RegistroListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub

		}

	}

}
