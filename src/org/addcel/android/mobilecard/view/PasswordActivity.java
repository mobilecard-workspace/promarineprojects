package org.addcel.android.mobilecard.view;

import java.util.List;

import org.addcel.android.crypto.Cifrados;
import org.addcel.android.font.Fonts;
import org.addcel.android.interfaces.Versionable;
import org.addcel.android.mobilecard.constant.Url;
import org.addcel.android.mobilecard.to.PasswordReq;
import org.addcel.android.promarine.R;
import org.addcel.android.promarine.view.MenuActivity;
import org.addcel.android.session.SessionManager;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;

public class PasswordActivity extends SherlockActivity implements Versionable {

	TextView passView, pActualView, confActualView, actualizarButton;
	EditText passText, pActualText, confActualText;

	SessionManager session;

	PasswordReq request;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_password);

		session = new SessionManager(PasswordActivity.this);
		request = getIntent().getParcelableExtra("request");
		
		initGUI();

		int version = android.os.Build.VERSION.SDK_INT;

		if (14 > version)
			setLowerVersionContent(passView, pActualView, confActualView,passText, pActualText, confActualText);
	}

	@Override
	public void setLowerVersionContent(TextView... view) {
		// TODO Auto-generated method stub

		Typeface type = Typeface.createFromAsset(getAssets(),
				Fonts.ROBOTO_REGULAR.getPath());

		for (TextView t : view)
			t.setTypeface(type);

	}
	
	private void initGUI() {
		passView = (TextView) findViewById(R.id.view_password);
		pActualView = (TextView) findViewById(R.id.view_pactual);
		confActualView = (TextView) findViewById(R.id.view_confactual);
		passText = (EditText) findViewById(R.id.text_password);
		pActualText = (EditText) findViewById(R.id.text_pactual);
		confActualText = (EditText) findViewById(R.id.text_confactual);
	}

	public void cancelar(View v) {
		finish();
	}

	public void actualizar(View v) {

		String pass = passText.getText().toString().trim();
		String pActual = pActualText.getText().toString().trim();
		String confActual = confActualText.getText().toString().trim();

		if (TextUtils.isEmpty(pass) || pass.length() < 8) {
			passText.requestFocus();
			passText.setError(ErrorUtil.notValid("Contrase�a Actual"));
		} else if (TextUtils.isEmpty(pActual) || pActual.length() < 8) {
			pActualText.requestFocus();
			pActualText.setError(ErrorUtil.notValid("Contrase�a Actual"));
		} else if (!TextUtils.equals(confActual, pActual)) {
			confActualText.requestFocus();
			confActualText.setError(ErrorUtil.notAMatch(
					"Confirmaci�n Contrase�a Nueva", "Contrase�a Nueva"));
		} else {
			request.setPasswordS(pass).setPassword(pActual);
			WSClient client = new WSClient(PasswordActivity.this)
					.hasLoader(true).setCancellable(false).setListener(new ActualizarListener())
					.setCifrado(Cifrados.MOBILECARD);

			if (request.getStatus() == 99) {

				client.setKey(pActual).setUrl(Url.USER_PASS_MAIL);

			} else {
				client.setKey(pass).setUrl(Url.UPDATE_PASS);
			}

			client.execute(request.toJSON().toString());
		}

	}
	
	private class ActualizarListener implements WSResponseListener {
		
		private static final String TAG = "ActualizarListener";

		@Override
		public void onStringReceived(String response) {
			// TODO Auto-generated method stub
			Toast.makeText(PasswordActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onJsonObjectReceived(JSONObject response) {
			// TODO Auto-generated method stub
			int resultado = response.optInt("resultado", 10000);
			String mensaje = response.optString("mensaje", ErrorUtil.connection());
			
			switch (resultado) {
			case 1:
				session.createLoginSession(String.valueOf(
						request.getId()),
						request.getLogin(),
						request.getPassword(), "");
				
				if (request.getStatus() == 1)
					finish();
				else
					startActivity(new Intent(PasswordActivity.this, MenuActivity.class));
				
				Toast.makeText(PasswordActivity.this, mensaje, Toast.LENGTH_LONG).show();
				break;

			default:
				Toast.makeText(PasswordActivity.this, mensaje, Toast.LENGTH_SHORT).show();
				break;
			}
		}

		@Override
		public void onJsonArrayReceived(JSONArray response) {
			// TODO Auto-generated method stub
			Toast.makeText(PasswordActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}

		@Override
		public void onCatalogReceived(List<BasicNameValuePair> catalog) {
			// TODO Auto-generated method stub
			Toast.makeText(PasswordActivity.this, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
			
		}
		
		
		
	}

}
