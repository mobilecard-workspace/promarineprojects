package org.addcel.android.dialog;

import org.addcel.android.crypto.Cifrados;
import org.addcel.android.mobilecard.constant.Url;
import org.addcel.android.promarine.R;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.ws.client.WSClient;
import org.addcel.android.ws.listener.WSResponseListener;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Dialogs {
	
	public static void recuperarDialog(final Context _con, final WSResponseListener _listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(_con)
									.setTitle("Recuperar contrase�a")
									.setMessage("Escribe tu nombre de usuario Mobilecard o tu correo electr�nico para recuperar tu contrase�a");
		
		LayoutInflater inflater = (LayoutInflater) _con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		final View recuperarView = inflater.inflate(R.layout.view_recuperar, null);
		
		final EditText datosText = (EditText) recuperarView.findViewById(R.id.text_dato);
		
		builder.setView(recuperarView)
				.setCancelable(true)
				.setPositiveButton("Recuperar", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						String datos = datosText.getText().toString().trim();
						
						
						if (TextUtils.isEmpty(datos)) {
							Toast.makeText(_con, ErrorUtil.isEmpty("nombre de usuario o correo electr�nico"), Toast.LENGTH_LONG).show();
						} else {
							
							try {
								
								JSONObject json = new JSONObject().put("cadena", datos);
								
								new WSClient(_con)
									.hasLoader(true)
									.setCifrado(Cifrados.HARD)
									.setListener(_listener)
									.setUrl(Url.RECOVERY_GET)
									.execute(json.toString());
								
							} catch (JSONException e) {
								Toast.makeText(_con, "Petici�n no v�lida", Toast.LENGTH_SHORT).show();
							}
						}
					}
				})
				.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				})
				.show();
	}
	
	public static void enviarDialog(final Context _con, final WSResponseListener _listener) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(_con)
									.setTitle("Reenv�o SMS")
									.setMessage("Escribe el n�mero de celular al que se reenviar� tu contrase�a");
		
		LayoutInflater inflater = (LayoutInflater) _con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		final View reenviarView = inflater.inflate(R.layout.view_reenviar, null);
		
		final EditText celularText = (EditText) reenviarView.findViewById(R.id.text_celular);
		
		builder.setView(reenviarView)
				.setCancelable(true)
				.setPositiveButton("Reenviar", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						String celular = celularText.getText().toString().trim();
						
						if (TextUtils.isEmpty(celular)) {
							Toast.makeText(_con, ErrorUtil.isEmpty("Celular"), Toast.LENGTH_LONG).show();
						} else if (!TextUtils.isDigitsOnly(celular) || celular.length() < 8) {
							Toast.makeText(_con, ErrorUtil.notValid("Celular"), Toast.LENGTH_LONG).show();
						} else {
							
							new WSClient(_con).hasLoader(true).setCifrado(Cifrados.HARD).setListener(_listener).setUrl(Url.SET_SMS).execute(celular);
						}
					}
				})
				.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				})
				.show();
		
	}
}
