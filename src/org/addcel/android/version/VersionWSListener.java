package org.addcel.android.version;

import java.util.List;

import org.addcel.android.interfaces.DialogOkListener;
import org.addcel.android.util.Dialogos;
import org.addcel.android.util.ErrorUtil;
import org.addcel.android.ws.listener.WSResponseListener;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

public class VersionWSListener implements WSResponseListener {
	
	Context context;
	Class<?> nextActivity;
	
	private static final String TAG = "VersionWSListener";
	
	public VersionWSListener(Context _context, Class<?> _nextActivity) {
		this.context = _context;
		this.nextActivity = _nextActivity;
	}
	
	public Context getContext() {
		return context;
	}
	
	public void setContext(Context _context) {
		this.context = _context;
	}
	
	public Class<?> getNextActivity() {
		return nextActivity;
	}
	
	public void setNextActivity(Class<?> _nextActivity) {
		this.nextActivity = _nextActivity;
	}

	@Override
	public void onStringReceived(String response) {
		// TODO Auto-generated method stub
		Toast.makeText(context, "Error de conexi�n. Intente de nuevo m�s tarde", Toast.LENGTH_SHORT).show();
		((SherlockActivity) context).finish();
	}

	@Override
	public void onJsonObjectReceived(JSONObject response) {
		// TODO Auto-generated method stub
		
		Log.i(TAG, response.toString());
		
		String version = getVersionName(context, nextActivity);
		
		String[] servVersion = response.optString("version").split("[.]");
		String[] currVersion = version.split("[.]");
		
		boolean pasa = true;
		
		for (int i = 0; i < servVersion.length; i++) {
			if (i < currVersion.length) {
				int max = Integer.parseInt(servVersion[i]);
				int min = Integer.parseInt(currVersion[i]);
				if (max > min) {
					pasa = false;
					break;
				}
			} else {
				break;
			}
		}
		
		if (pasa) {
			Intent intent = new Intent(context, nextActivity);
			context.startActivity(intent);
		} else {
			String prior = response.optString("tipo");
			final String url = response.optString("url");
			
			if (prior.equals("1")) {
				Dialogos.makeOkAlert(context, "Hay nueva nueva versi�n importante. \n"+
						" su versi�n: "+version+"\n"+
						" nueva versi�n: "+response.optString("version"), new DialogOkListener() {
							
							@Override
							public void ok(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setData(Uri.parse(url));
								context.startActivity(intent);
							}
							
							@Override
							public void cancel(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
							}
						});
			} else {
				Dialogos.makeYesNoAlert(context, "Hay nueva nueva versi�n disponible. \n "+
						" su version: "+version+"\n"+
						" nueva version: "+response.optString("version")+"\n"+
						" �Desea Descargarla?", new DialogOkListener() {
							
							@Override
							public void ok(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setData(Uri.parse(url));
								context.startActivity(intent);
							}
							
							@Override
							public void cancel(DialogInterface dialog, int id) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(context, nextActivity);
								context.startActivity(intent);
							}
						});
			}
		}
	}

	@Override
	public void onJsonArrayReceived(JSONArray response) {
		// TODO Auto-generated method stub
		Toast.makeText(context, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
		((SherlockActivity) context).finish();
		
	}

	@Override
	public void onCatalogReceived(List<BasicNameValuePair> catalog) {
		// TODO Auto-generated method stub
		Toast.makeText(context, ErrorUtil.connection(), Toast.LENGTH_SHORT).show();
		((SherlockActivity) context).finish();
		
	}
	

	
	private String getVersionName(Context context, Class<?> cls) {
      try {
        ComponentName comp = new ComponentName(context, cls);
        PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
        return pinfo.versionName;
      } catch (android.content.pm.PackageManager.NameNotFoundException e) {
        return null;
      }
    }

	
}
